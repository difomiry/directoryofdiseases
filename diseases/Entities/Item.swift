
import Foundation

struct Item {
    
    var id: Int
    var name: String
    var category: Int
    
    init(id: Int, name: String, category: Int) {
        self.id = id
        self.name = name
        self.category = category
    }
    
}
