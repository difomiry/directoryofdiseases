
import Foundation
import RxDataSources
import RxCocoa
import RxSwift

struct CategoryWithItems {

    var id: Int
    var name: String
    var items: [Item]
    
    init(id: Int, name: String, items: [Item]) {
        self.id = id
        self.name = name
        self.items = items
    }

}

extension CategoryWithItems: SectionModelType {

    init(original: CategoryWithItems, items: [Item]) {
        self = original
        self.items = items
    }
    
}
