
import Foundation

struct Content {
    
    var header: String
    var data: String
    
    init(header: String, data: String) {
        self.header = header
        self.data = data
    }
    
}
