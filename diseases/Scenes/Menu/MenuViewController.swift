
import UIKit

class MenuViewController: BaseViewController {
    
    var customView: MenuView { return view as! MenuView }

    
    override init() {
        
        super.init()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = MenuView()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        title = "Directory".localized()
        
        customView.tabBarView.delegate = self

    }

}
