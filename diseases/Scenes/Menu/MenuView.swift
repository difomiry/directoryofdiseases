
import UIKit
import SnapKit

class MenuView: BaseView {
    
    var redButton: CardView
    var redButtonLabel: UILabel
    var redButtonImage: UIImageView
    var greenButton: CardView
    var greenButtonLabel: UILabel
    var blueButton: CardView
    var blueButtonLabel: UILabel
    var tabBarView: TabBarView
    var activityIndicatorView: UIActivityIndicatorView
    
    override init() {
        
        redButton = CardView()
        redButtonLabel = UILabel()
        redButtonImage = UIImageView(image: UIImage(named: "Diseases")?.withRenderingMode(.alwaysOriginal))
        greenButton = CardView()
        greenButtonLabel = UILabel()
        blueButton = CardView()
        blueButtonLabel = UILabel()
        tabBarView = TabBarView()
        activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        
        super.init()
        
        addSubview(redButton)
        redButton.addSubview(redButtonLabel)
        redButton.addSubview(redButtonImage)
        addSubview(greenButton)
        greenButton.addSubview(greenButtonLabel)
        addSubview(blueButton)
        blueButton.addSubview(blueButtonLabel)
        addSubview(tabBarView)
        addSubview(activityIndicatorView)
        
        redButton.backgroundColor = UIColor(red: 253/255, green: 67/255, blue: 56/255, alpha: 1)
        redButton.cornerRadius = 12
        greenButton.backgroundColor = UIColor(red: 255/255, green: 217/255, blue: 97/255, alpha: 1)
        greenButton.cornerRadius = 12
        blueButton.backgroundColor = UIColor(red: 138/255, green: 223/255, blue: 81/255, alpha: 1)
        blueButton.cornerRadius = 12
        
        redButtonLabel.text = "Diseases".localized()
        redButtonLabel.font = UIFont.boldSystemFont(ofSize: 17)
        redButtonLabel.textColor = .white
        greenButtonLabel.text = "Diseases".localized()
        greenButtonLabel.font = UIFont.boldSystemFont(ofSize: 17)
        greenButtonLabel.textColor = .white
        blueButtonLabel.text = "Diseases".localized()
        blueButtonLabel.font = UIFont.boldSystemFont(ofSize: 17)
        blueButtonLabel.textColor = .white
        
        redButton.snp.makeConstraints {
            $0.top.leading.equalTo(safeAreaLayoutGuide).offset(10)
            $0.trailing.equalTo(safeAreaLayoutGuide).offset(-10)
            $0.height.equalTo(120)
        }
        
        redButtonLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(15)
        }
        
        greenButton.snp.makeConstraints {
            $0.leading.equalTo(safeAreaLayoutGuide).offset(10)
            $0.top.equalTo(redButton.snp.bottom).offset(10)
            $0.trailing.equalTo(safeAreaLayoutGuide.snp.centerX).offset(-5)
            $0.height.equalTo(120)
        }
        
        greenButtonLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(15)
        }
        
        blueButton.snp.makeConstraints {
            $0.leading.equalTo(safeAreaLayoutGuide.snp.centerX).offset(5)
            $0.top.equalTo(redButton.snp.bottom).offset(10)
            $0.trailing.equalTo(safeAreaLayoutGuide).offset(-10)
            $0.height.equalTo(120)
        }
        
        blueButtonLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(15)
        }
        
        redButtonImage.snp.makeConstraints {
            $0.top.bottom.trailing.equalToSuperview()
            $0.leading.equalToSuperview().offset(60)
        }
        
        tabBarView.snp.makeConstraints {
            $0.bottom.leading.trailing.equalTo(safeAreaLayoutGuide)
            $0.height.equalTo(60)
        }
        
        activityIndicatorView.snp.makeConstraints {
            $0.centerX.centerY.equalTo(safeAreaLayoutGuide)
            $0.width.equalTo(80)
            $0.height.equalTo(80)
        }
        
        show()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func show() {
        tabBarView.isHidden = false
        activityIndicatorView.isHidden = true
        activityIndicatorView.stopAnimating()
    }
    
    func hide() {
        tabBarView.isHidden = true
        activityIndicatorView.isHidden = false
        activityIndicatorView.startAnimating()
    }
    
}
