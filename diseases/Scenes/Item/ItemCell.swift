
import UIKit

class ItemCell: UITableViewCell {
    
    let headerLabel: UILabel
    let dataLabel: UILabel
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        headerLabel = UILabel()
        dataLabel = UILabel()
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        customize(contentView)
        customize(headerLabel)
        customize(dataLabel)
        
        layout(headerLabel)
        layout(dataLabel)
        
        constrain(headerLabel)
        constrain(dataLabel)
    }
    
    func customize(_ view: UIView) {
        switch view {
        case contentView:
            selectionStyle = .none
            contentView.backgroundColor = .white
        case headerLabel:
            headerLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
            headerLabel.textColor = .black
            headerLabel.numberOfLines = 0
        case dataLabel:
            dataLabel.font = UIFont.systemFont(ofSize: 15.0)
            dataLabel.textColor = .black
            dataLabel.numberOfLines = 0
        default:
            break
        }
    }
    
    func layout(_ view: UIView) {
        switch view {
        default:
            contentView.addSubview(view)
        }
    }
    
    func constrain(_ view: UIView) {
        view.snp.makeConstraints {
            switch view {
            case headerLabel:
                $0.top.equalTo(contentView)
                $0.leading.equalTo(contentView).offset(20)
                $0.trailing.equalTo(contentView).offset(-20)
                $0.bottom.equalTo(dataLabel.snp.top).offset(-15)
            case dataLabel:
                $0.top.equalTo(headerLabel.snp.bottom)
                $0.leading.equalTo(contentView).offset(20)
                $0.trailing.equalTo(contentView).offset(-20)
                $0.bottom.equalTo(contentView).offset(-15)
            default:
                break
            }
        }
    }
    
    func bind(_ index: Int, _ content: Content) {
        
        headerLabel.snp.remakeConstraints {
            if index == 0 {
                $0.top.equalTo(contentView).offset(15)
                $0.leading.equalTo(contentView).offset(20)
                $0.trailing.equalTo(contentView).offset(-20)
                $0.bottom.equalTo(dataLabel.snp.top).offset(-15)
            } else {
                $0.top.equalTo(contentView)
                $0.leading.equalTo(contentView).offset(20)
                $0.trailing.equalTo(contentView).offset(-20)
                $0.bottom.equalTo(dataLabel.snp.top).offset(-15)
            }
        }
        
        headerLabel.text = content.header
        dataLabel.text = content.data
    }
}
