
import UIKit
import SnapKit

class ItemView: BaseView {
    
    
    var tableView: UITableView
    var activityIndicatorView: UIActivityIndicatorView
    
    override init() {
        
        tableView = UITableView()
        activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        
        super.init()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.backgroundView?.backgroundColor = .white
        tableView.backgroundColor = .white
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.layoutMargins = .zero
        tableView.separatorInset = .zero
        tableView.indexDisplayMode = .automatic
        tableView.register(ItemCell.self, forCellReuseIdentifier: "itemCell")
        
        addSubview(tableView)
        addSubview(activityIndicatorView)
        
        tableView.snp.makeConstraints {
            $0.top.bottom.leading.trailing.equalTo(safeAreaLayoutGuide)
        }
        
        activityIndicatorView.snp.makeConstraints {
            $0.centerX.centerY.equalTo(safeAreaLayoutGuide)
            $0.width.equalTo(80)
            $0.height.equalTo(80)
        }
        
        hide()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func show() {
        tableView.isHidden = false
        activityIndicatorView.isHidden = true
        activityIndicatorView.stopAnimating()
    }
    
    func hide() {
        tableView.isHidden = true
        activityIndicatorView.isHidden = false
        activityIndicatorView.startAnimating()
    }
    
}
