
import Foundation
import RxSwift

class ItemViewModel {
    
    let diseasesService: DiseasesService
    
    let id: Int
    
    init(_ diseasesService: DiseasesService, _ id: Int) {
        self.diseasesService = diseasesService
        self.id = id
    }
    
    func content() -> Observable<[Content]> {
        return diseasesService.getDiseaseContent(id: id)
    }
    
}
