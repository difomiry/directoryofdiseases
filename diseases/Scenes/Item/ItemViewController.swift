
import UIKit
import RxSwift
import RxCocoa

class ItemViewController: BaseViewController {
    
    var customView: ItemView { return view as! ItemView }
    
    let viewModel: ItemViewModel
    
    let bag = DisposeBag()
    
    init(_ viewModel: ItemViewModel) {
        
        self.viewModel = viewModel
        
        super.init()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = ItemView()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        title = "Item".localized()
        
        viewModel
            .content()
            .do { self.customView.show() }
            .bind(to: customView.tableView.rx.items(cellIdentifier: "itemCell", cellType: ItemCell.self)) {
                $2.bind($0, $1)
            }
            .disposed(by: bag)
        
    }
    
}
