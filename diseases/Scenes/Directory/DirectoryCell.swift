
import UIKit

class DirectoryCell: UITableViewCell {
    
    let nameLabel: UILabel
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        nameLabel = UILabel()
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        customize(contentView)
        customize(nameLabel)
        
        layout(nameLabel)
        
        constrain(nameLabel)
    }
    
    func customize(_ view: UIView) {
        switch view {
        case contentView:
            selectionStyle = .none
            contentView.backgroundColor = .white
            accessoryType = .disclosureIndicator
        case nameLabel:
            nameLabel.font = UIFont.systemFont(ofSize: 17.0)
            nameLabel.textColor = .black
            nameLabel.numberOfLines = 0
        default:
            break
        }
    }
    
    func layout(_ view: UIView) {
        switch view {
        default:
            contentView.addSubview(view)
        }
    }
    
    func constrain(_ view: UIView) {
        view.snp.makeConstraints {
            switch view {
            case nameLabel:
                $0.top.equalTo(20)
                $0.bottom.equalTo(-20)
                $0.centerY.equalTo(self)
                $0.leading.equalTo(self).offset(20)
                $0.trailing.equalTo(self).offset(40)
                $0.width.equalTo(self).offset(-60)
            default:
                break
            }
        }
    }
    
    func bind(_ item: Item) {
        nameLabel.text = item.name
    }
}
