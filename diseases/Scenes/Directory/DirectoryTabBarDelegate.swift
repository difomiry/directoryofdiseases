
import UIKit
import Localize_Swift

extension DirectoryViewController: UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        // clear selection
        tabBar.selectedItem = nil
        
        switch tabBar.items?.index(of: item) {
        case 2:
            let alert = UIAlertController(title: nil, message: "Switch language".localized(), preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {
                (alert: UIAlertAction) -> Void in
            })
            
            for language in Localize.availableLanguages() {
                if (language == "ru" || language == "en") {
                    let displayName = Localize.displayNameForLanguage(language)
                    let languageAction = UIAlertAction(title: displayName, style: .default, handler: {
                        (alert: UIAlertAction!) -> Void in
                        if (Localize.currentLanguage() != language) {
                            Localize.setCurrentLanguage(language)
                            let window = (UIApplication.shared.delegate?.window!)
                            var rootViewController: UIViewController
                            if Localize.currentLanguage() == "ru" {
                                rootViewController = Navigator.toMenu(DiseasesService(ApiService())).controller
                            } else {
                                rootViewController = Navigator.toDirectory(DiseasesService(ApiService())).controller
                            }
                            let navigationController = UINavigationController(rootViewController: rootViewController)
                            window?.rootViewController = navigationController
                            window?.makeKeyAndVisible()

                        }
                    })
                    alert.addAction(languageAction)
                }
            }
            
            alert.addAction(cancelAction)
            
            present(alert, animated: true)
        default:
            return
        }
    }
    
}
