
import Foundation
import RxSwift

class DirectoryViewModel {
    
    let diseasesService: DiseasesService
    
    init(_ diseasesService: DiseasesService) {
        self.diseasesService = diseasesService
    }
    
    func items() -> Observable<[CategoryWithItems]> {
        return diseasesService
            .getDiseaseCategoriesWithItems()
            .map { $0.sorted { $0.id < $1.id } }
    }
    
}
