
import UIKit
import SnapKit

class DirectoryView: BaseView {

    
    var tableView: UITableView
    var tabBarView: TabBarView
    var activityIndicatorView: UIActivityIndicatorView
    
    override init() {
        
        tableView = UITableView()
        tabBarView = TabBarView()
        activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        
        super.init()
        
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.backgroundView?.backgroundColor = .white
        tableView.backgroundColor = .white
        tableView.layoutMargins = .zero
        tableView.separatorInset = .zero
        tableView.indexDisplayMode = .automatic
        tableView.register(DirectoryCell.self, forCellReuseIdentifier: "directoryCell")
        
        addSubview(tableView)
        addSubview(tabBarView)
        addSubview(activityIndicatorView)
        
        tableView.snp.makeConstraints {
            $0.top.leading.trailing.equalTo(safeAreaLayoutGuide)
            $0.bottom.equalTo(tabBarView.snp.top)
        }
        
        tabBarView.snp.makeConstraints {
            $0.bottom.leading.trailing.equalTo(safeAreaLayoutGuide)
            $0.top.equalTo(tableView.snp.bottom)
            $0.height.equalTo(60)
        }
            
        activityIndicatorView.snp.makeConstraints {
            $0.centerX.centerY.equalTo(safeAreaLayoutGuide)
            $0.width.equalTo(80)
            $0.height.equalTo(80)
        }
        
        hide()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func show() {
        tableView.isHidden = false
        tabBarView.isHidden = false
        activityIndicatorView.isHidden = true
        activityIndicatorView.stopAnimating()
    }
    
    func hide() {
        tableView.isHidden = true
        tabBarView.isHidden = true
        activityIndicatorView.isHidden = false
        activityIndicatorView.startAnimating()
    }
    
}
