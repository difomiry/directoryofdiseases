
import UIKit
import RxDataSources
import RxSwift
import RxCocoa

class DirectoryViewController: BaseViewController {

    var customView: DirectoryView { return view as! DirectoryView }
    
    let viewModel: DirectoryViewModel
    
    let bag = DisposeBag()
    
    init(_ viewModel: DirectoryViewModel) {
        
        self.viewModel = viewModel
        
        super.init()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = DirectoryView()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    
        title = "Directory".localized()
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.makeBorder()
        
        navigationItem.hidesSearchBarWhenScrolling = true
        navigationItem.searchController = searchController
        
        customView.tabBarView.delegate = self
        
        let dataSource = RxTableViewSectionedReloadDataSource<CategoryWithItems>(
            configureCell: { (_, tv, indexPath, element) in
                let cell = tv.dequeueReusableCell(withIdentifier: "directoryCell") as! DirectoryCell
                cell.bind(element)
                return cell
            },
            titleForHeaderInSection: { dataSource, sectionIndex in
                return dataSource[sectionIndex].name
            }
        )
        
        viewModel
            .items()
            .do { self.customView.show() }
            .bind(to: customView.tableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        customView.tableView.rx
            .modelSelected(Item.self)
            .subscribe(onNext: { self.navigate(.toItem(self.viewModel.diseasesService, $0.id)) })
            .disposed(by: bag)
    
    }
    
}
