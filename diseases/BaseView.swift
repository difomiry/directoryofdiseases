
import UIKit
import SnapKit

class BaseView: UIView {
    
    init() {
        
        super.init(frame: .zero)
        
        self.backgroundColor = .white
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
