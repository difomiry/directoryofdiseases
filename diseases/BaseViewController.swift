
import UIKit

class BaseViewController: UIViewController {
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func navigate(_ to: Navigator) {
        navigationController?.pushViewController(to.controller, animated: true)
    }
    
}
