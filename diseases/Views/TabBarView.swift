
import UIKit

class TabBarView: UITabBar {
    
    init() {
        
        super.init(frame: .zero)
        
        var items = [UITabBarItem]()
        
        items.append(UITabBarItem(title: "Bookmarks".localized(), image: UIImage(named: "Bookmarks")?.withRenderingMode(.alwaysOriginal), tag: 0))
        items.append(UITabBarItem(title: "History".localized(), image: UIImage(named: "History")?.withRenderingMode(.alwaysOriginal), tag: 1))
        items.append(UITabBarItem(title: "Language".localized(), image: UIImage(named: "Language")?.withRenderingMode(.alwaysOriginal), tag: 2))
        items.append(UITabBarItem(title: "About".localized(), image: UIImage(named: "About")?.withRenderingMode(.alwaysOriginal), tag: 3))
        
        setItems(items, animated: true)
        
        itemPositioning = .fill
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
