
import Foundation
import UIKit

enum Navigator {
    
    case toMenu(DiseasesService)
    case toDirectory(DiseasesService)
    case toItem(DiseasesService, Int)
    
    var controller: UIViewController {
        switch self {
        case .toMenu(let diseasesService):
            return MenuViewController()
        case .toDirectory(let diseasesService):
            return DirectoryViewController(DirectoryViewModel(diseasesService))
        case .toItem(let diseasesService, let id):
            return ItemViewController(ItemViewModel(diseasesService, id))
        }
    }
    
}
