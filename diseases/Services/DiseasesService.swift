
import Foundation
import RxSwift
import RxCocoa
import Localize_Swift

class DiseasesService {
    
    private let apiService: ApiService
    
    init(_ apiService: ApiService) {
        self.apiService = apiService
    }
    
    func timestamp() -> Int {
        return UserDefaults.standard.integer(forKey: "ru.involta.diseases.disease.timestamp")
    }
    
    func timestamp(_ timestamp: Int) {
        UserDefaults.standard.set(timestamp, forKey: "ru.involta.diseases.disease.timestamp")
    }
    
    func getDiseaseNames() -> Observable<[Item]> {
        return apiService.getDiseaseNames(0, Localize.currentLanguage())
    }
    
    func getDiseaseCategories() -> Observable<[Category]> {
        return apiService.getDiseaseCategories(0, Localize.currentLanguage())
    }
    
    func getDiseaseContent(id: Int) -> Observable<[Content]> {
        return apiService.getDiseaseContent(id, Localize.currentLanguage())
    }
    
    func getCategoryWithItems(id: Int, name: String) -> Observable<CategoryWithItems> {
        return apiService.getCategoryWithItems(id, name, Localize.currentLanguage())
    }
    
    func getDiseaseCategoriesWithItems() -> Observable<[CategoryWithItems]> {
        return apiService
            .getDiseaseCategories(0, Localize.currentLanguage())
            .flatMap { categories -> Observable<[CategoryWithItems]> in
                let categoriesObservable = Observable.from(categories)
                let categoriesWithItemsObservable = categoriesObservable
                    .flatMap { self.getCategoryWithItems(id: $0.id, name: $0.name) }
                    .filter { !$0.items.isEmpty }
                
                return categoriesWithItemsObservable.toArray()
            }
    }
    
}
