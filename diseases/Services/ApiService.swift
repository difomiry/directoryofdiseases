
import Foundation
import RxSwift
import RxAlamofire
import SwiftyJSON

class ApiService {
    
    private func request(_ request: ApiRequest) -> Observable<Any> {
        return RxAlamofire
            .json(request.method, request.path, parameters: request.parameters)
            .debug()
    }
    
    func getDiseaseNames(_ timestamp: Int, _ localization: String) -> Observable<[Item]> {
        return request(.getDiseaseNames(timestamp, localization))
            .map { json -> [Item] in
                var items = [Item]()
                for disease in JSON(json)["diseases"].arrayValue {
                    items.append(Item(
                        id: disease["id"].intValue,
                        name: disease["name"].stringValue,
                        category: disease["category"].intValue
                    ))
                }
                return items
        }
    }
    
    func getDiseaseCategories(_ timestamp: Int, _ localization: String) -> Observable<[Category]> {
        return request(.getDiseaseCategories(timestamp, localization))
            .map { json -> [Category] in
                var categories = [Category]()
                for category in JSON(json)["categories"].arrayValue {
                    categories.append(Category(
                        id: category["id"].intValue,
                        name: category["name"].stringValue
                    ))
                }
                return categories
        }
    }
    
    func getDiseaseContent(_ id: Int, _ localization: String) -> Observable<[Content]> {
        return request(.getDiseaseContent(id, localization))
            .map { json -> [Content] in
                var contents = [Content]()
                let dictionary = JSON(json)
                if !dictionary["description"].stringValue.isEmpty {
                    contents.append(Content(
                        header: "Description".localized(),
                        data: dictionary["description"].stringValue
                    ))
                }
                if !dictionary["symptoms"].stringValue.isEmpty {
                    contents.append(Content(
                        header: "Symptoms".localized(),
                        data: dictionary["symptoms"].stringValue
                    ))
                }
                for content in dictionary["blocks"].arrayValue {
                    contents.append(Content(
                        header: content["header"].stringValue,
                        data: content["data"].stringValue
                    ))
                }
                return contents
        }
    }
    
    func getCategoryWithItems(_ id: Int, _ name: String, _ localization: String) -> Observable<CategoryWithItems> {
        return request(.getCategoryWithItems(id, localization))
            .map { json -> CategoryWithItems in
                var category = CategoryWithItems(id: id, name: name, items: [Item]())
                for item in JSON(json)["diseases"].arrayValue {
                    category.items.append(Item(
                        id: item["id"].intValue,
                        name: item["name"].stringValue,
                        category: item["category"].intValue
                    ))
                }
                return category
        }
    }
    
}
