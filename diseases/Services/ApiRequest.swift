
import Foundation
import Alamofire
import RxAlamofire

enum ApiRequest {
    
    case getDiseaseCategories(Int, String)
    case getDiseaseNames(Int, String)
    case getDiseaseContent(Int, String)
    case getCategoryWithItems(Int, String)
    
    case search(String)
    
}

extension ApiRequest {
    
    var method: HTTPMethod {
        switch self {
        case .getDiseaseCategories:
            return .post
        case .getDiseaseNames:
            return .post
        case .getDiseaseContent:
            return .post
        case .getCategoryWithItems:
            return .post
        case .search:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .getDiseaseCategories:
            return "http://directorydiseases.ru/api6/disease/getCategories"
        case .getDiseaseNames:
            return "http://directorydiseases.ru/api6/disease/getNames"
        case .getDiseaseContent:
            return "http://directorydiseases.ru/api6/disease/getDiseaseById"
        case .getCategoryWithItems:
            return "http://directorydiseases.ru/api6/disease/getDiseasesByCategoryId"
        case .search:
            return "http://directorydiseases.ru/api6/disease/search"
        }
    }
    
    var parameters: [String: Any] {
        switch self {
        case .getDiseaseCategories(let timestamp, let localization):
            return ["timestamp": timestamp, "localization": localization]
        case .getDiseaseNames(let timestamp, let localization):
            return ["timestamp": timestamp, "localization": localization]
        case .getDiseaseContent(let id, let localization):
            return ["id": id, "localization": localization]
        case .getCategoryWithItems(let id, let localization):
            return ["id": id, "localization": localization]
        case .search(let query):
            return ["query": query]
        }
    }
    
}
