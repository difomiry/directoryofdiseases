
import UIKit
import Localize_Swift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)
        
        var rootViewController: UIViewController
        
        if Localize.currentLanguage() == "ru" {
            rootViewController = Navigator.toMenu(DiseasesService(ApiService())).controller
        } else {
            rootViewController = Navigator.toDirectory(DiseasesService(ApiService())).controller
        }
        
        let navigationController = UINavigationController(rootViewController: rootViewController)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        return true
    }

}
