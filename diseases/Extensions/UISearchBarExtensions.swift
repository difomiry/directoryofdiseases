
import UIKit

extension UISearchBar {
    
    func makeBorder(_ cornerRadius: CGFloat = 10, _ borderWidth: CGFloat = 2, _ borderColor: UIColor = UIColor(red:0.17, green:0.48, blue:1.00, alpha:1.0)) {
        for v in subviews  {
            for vv in v.subviews  {
                if let textField = vv as? UITextField {
                    var bounds: CGRect
                    bounds = textField.frame
                    textField.bounds = bounds
                    textField.layer.cornerRadius = cornerRadius
                    textField.layer.borderWidth = borderWidth
                    textField.layer.borderColor = borderColor.cgColor
                    textField.backgroundColor = UIColor.white
                }
            }
        }
    }
    
}
