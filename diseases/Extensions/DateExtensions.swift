
import Foundation

extension Date {
    
    func timestamp() -> Int {
        return Int(timeIntervalSince1970.rounded())
    }
    
}
